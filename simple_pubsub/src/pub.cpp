#include <chrono>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "simple_pubsub/msg/stamped_num.hpp"

using namespace std::chrono_literals;

/* This example creates a subclass of Node and uses std::bind() to register a
* member function as a callback from the timer. */

class Pub : public rclcpp::Node
{
  public:
    Pub()
    : Node("pub")
    {
        auto param_desc = rcl_interfaces::msg::ParameterDescriptor();
        param_desc.description = "Inter-message period in microseconds";
        this->declare_parameter("period", 100000, param_desc);

        this->timer_period_ = std::chrono::microseconds(this->get_parameter("period").as_int());
        this->count_ = 0;

        auto pub_qos = rclcpp::QoS(rclcpp::KeepLast(50)).best_effort().transient_local();

        publisher_ = this->create_publisher<simple_pubsub::msg::StampedNum>("topic", pub_qos);
        timer_ = rclcpp::create_timer(this, this->get_clock(), this->timer_period_, std::bind(&Pub::timer_callback, this));
    }

  private:
    void timer_callback();

    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<simple_pubsub::msg::StampedNum>::SharedPtr publisher_;
    uint32_t count_;
    std::chrono::microseconds timer_period_;
};

void Pub::timer_callback(){
    auto message = simple_pubsub::msg::StampedNum();
    message.header.stamp.sec = this->now().seconds();
    message.header.stamp.nanosec = this->now().nanoseconds();
    message.num = this->count_;

    RCLCPP_INFO(this->get_logger(), "[%d] Publishing message '%d'",message.header.stamp.sec, message.num);
    publisher_->publish(message);

    this->count_++;
}

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<Pub>());
  rclcpp::shutdown();
  return 0;
}