#include <fstream>
#include <iostream>

#include "rclcpp/rclcpp.hpp"
#include "simple_pubsub/msg/stamped_num.hpp"

#include <boost/filesystem.hpp>

using std::placeholders::_1;

class Sub : public rclcpp::Node
{
  public:
    Sub()
    : Node("sub")
    {
        auto param_desc = rcl_interfaces::msg::ParameterDescriptor();
        param_desc.description = "Name of the experiment for data saving";
        this->declare_parameter("exp_name", "trash", param_desc);

        auto sub_qos = rclcpp::QoS(rclcpp::KeepLast(50)).best_effort().transient_local();

        subscription_ = this->create_subscription<simple_pubsub::msg::StampedNum>(
        "/topic", sub_qos, std::bind(&Sub::topic_callback, this, _1));

// vvvvvvvvvvvvvvvvvvvvvvvv DATA SAVING vvvvvvvvvvvvvvvvvvvvvvvv

    // Create a folder based on the experience name, if not existant already
    std::string experience_name = this->get_parameter("exp_name").as_string();
    if(boost::filesystem::create_directories("./data/"+experience_name)){
        RCLCPP_DEBUG(this->get_logger(), "Created a new data folder for this experience : %s", experience_name.c_str());
    } else {
        RCLCPP_DEBUG(this->get_logger(), "Using existing data folder for this experiment");
    }

    // Define the output file name, based on the existing files in the experience folder (incremental)
    std::string temp_path;
    int i = 1;
    while(this->m_output_file.empty()){
        temp_path = "./data/"+experience_name+"/receiver_data"+std::to_string(i)+".csv";
        if(boost::filesystem::exists(temp_path)){
            i++;
        } else {
            this->m_output_file = temp_path;
        }
    }

    // initialize the output file with headers
    std::ofstream f;
    f.open(this->m_output_file.c_str(), std::ios::out);
    f << "Time,MsgTimestamp,Num"
    << std::endl;
    f.close();

// ^^^^^^^^^^^^^^^^^^^^^^^^ DATA SAVING ^^^^^^^^^^^^^^^^^^^^^^^^

    }

  private:
    void topic_callback(const simple_pubsub::msg::StampedNum & msg) const;

    rclcpp::Subscription<simple_pubsub::msg::StampedNum>::SharedPtr subscription_;

    std::string m_output_file;
    
};

void Sub::topic_callback(const simple_pubsub::msg::StampedNum & msg) const
{
    std::ofstream f;
    f.open(this->m_output_file.c_str(), std::fstream::app);
    f << std::setprecision(20)
    << this->now().nanoseconds() << ","
    << msg.header.stamp.sec * 1e9 + msg.header.stamp.nanosec << ","
    << msg.num
    << std::endl;
    f.close();
    RCLCPP_INFO(this->get_logger(), "[%f] I heard: '%d'",this->now().seconds(), msg.num);
}

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<Sub>());
  rclcpp::shutdown();
  return 0;
}