import os
import yaml
from launch import LaunchDescription
from launch.actions import ExecuteProcess, LogInfo
from launch_ros.actions import Node


def generate_launch_description():
    ws_path = '/home/theotime/simulation_ws'
    config_path = ws_path + '/src/config/config_flocking_2.yaml'

    # copy all the environment variables of the user
    node_env = os.environ.copy()

    # read the config file (Need pyYaml >= 5.1)
    with open(config_path) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    def flocking_instance(i):
        return Node(
            package='offboard_flocking',
            executable='VAT_flocking',
            name='vat_flocking_' + str(i),
            parameters=[
                {"config_file": config_path},
                {"robot_id": i},
                {"use_sim_time": False},
            ]
        )
    
    def flocking_instance_netns(i):
        return ExecuteProcess(
            cmd=['netns-exec', 'net'+str(i), 'bash', '-c', 'source install/setup.bash && ros2 run offboard_flocking VAT_flocking --ros-args -p config_file:='+config_path+' -p robot_id:='+str(i+1)+' -p use_sim_time:=true'],
            name='vat_flocking_' + str(i),
        )
        

    ld = LaunchDescription([
        LogInfo(msg='Launching flocking nodes...')
    ])

    for i in range(1, config['robots_number']+1):
        ld.add_action(flocking_instance(i))

    return ld