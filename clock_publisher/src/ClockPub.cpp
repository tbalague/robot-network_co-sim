#include <cstdio>
#include <rclcpp/rclcpp.hpp>
#include "rosgraph_msgs/msg/clock.hpp"

class ClockPub : public rclcpp::Node
{
  public:
    ClockPub() : Node("clock_pub")
    {
      auto param_desc = rcl_interfaces::msg::ParameterDescriptor();
      
      param_desc.description = "Frequency of clock publication";
      this->declare_parameter("frequency", 100, param_desc);

      param_desc.description = "Real-Time Factor";
      this->declare_parameter("rtf", 1.0, param_desc);

      auto clock_qos = rclcpp::QoS(rclcpp::KeepLast(1)).best_effort();
      this->clock_publisher_ = this->create_publisher<rosgraph_msgs::msg::Clock>("/clock", clock_qos);

      this->current_sim_time = rclcpp::Time(0);

      this->timer_period_ = std::chrono::microseconds(1/this->get_parameter("frequency").as_int());
      this->timer_ = rclcpp::create_timer(this, this->get_clock(), this->timer_period_, std::bind(&ClockPub::timer_callback, this));
    }

  private:
    void timer_callback();
    rclcpp::Publisher<rosgraph_msgs::msg::Clock>::SharedPtr clock_publisher_;
    rclcpp::TimerBase::SharedPtr timer_;
    std::chrono::microseconds timer_period_; // us
    rclcpp::Time current_sim_time; // us
};

void
ClockPub::timer_callback()
{
  rosgraph_msgs::msg::Clock clock_msg;
  clock_msg.set__clock(this->current_sim_time);
  this->clock_publisher_->publish(clock_msg);
  this->current_sim_time += this->timer_period_ * this->get_parameter("rtf").as_double();
}


int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<ClockPub>());
  rclcpp::shutdown();
  return 0;
}