            // PX4 SITL sensors
            this->vehicle_status_sub_ = this->create_subscription<px4_msgs::msg::VehicleStatus>(
                "/px4_"+std::to_string(robot_id)+"/fmu/out/vehicle_status",
                qos_sub, 
                std::bind(&VATPilot::vehicle_status_clbk, this, _1)
            );
            this->raw_obstacle_sub_ = this->create_subscription<obstacle_detector::msg::Obstacles>(
                "/raw_obstacles",
                qos_sub,
                std::bind(&VATPilot::raw_obstacle_clbk, this, _1)
            );
            this->lidar_sub_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
                "/lidar",
                qos_sub,
                std::bind(&VATPilot::lidar_clbk, this, _1)
            );

            // this->uwb_sub_ = this->create_subscription<ros_net_sim_interfaces::msg::Uwb>(
            //     "uwb",
            //     qos_sub,
            //     std::bind(&VATPilot::uwb_clbk, this, _1)
            // );

            // Subscribe to the position of all the robots
            std::string topic_name;
            for(int i = 0; i < num_robots; i++){
                topic_name = "/px4_"+std::to_string(i+1)+"/fmu/out/vehicle_local_position";
                this->peer_positions_subs_.push_back(this->create_subscription<px4_msgs::msg::VehicleLocalPosition>(
                                            topic_name, 
                                            qos_sub,
                                            [this, i](const px4_msgs::msg::VehicleLocalPosition &pose){this->pose_clbk(pose, i);}
                ));
                RCLCPP_INFO(this->get_logger(), "Subscribed to: %s", topic_name.c_str());
            }



void VATPilot::raw_obstacle_clbk(const obstacle_detector::msg::Obstacles & _msg){
    this->known_raw_obstacles = _msg;
    RCLCPP_DEBUG(this->get_logger(), "Saved obstacles data");
}

void VATPilot::lidar_clbk(const sensor_msgs::msg::LaserScan & data){
    this->lidar_msg = data;
    RCLCPP_DEBUG(this->get_logger(), "Received Lidar data");
}

// void VATPilot::uwb_clbk(const ros_net_sim_interfaces::msg::Uwb & data){
//     this->msgs_uwb.push_back(data);
// }