/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// 
// From ns-3 : src/tap-bridge/examples/tap-csma-virtual-machine.cc
// 



//
// This is an illustration of how one could use virtualization techniques to
// allow running applications on virtual machines talking over simulated
// networks.
//
// The actual steps required to configure the virtual machines can be rather
// involved, so we don't go into that here.  Please have a look at one of
// our HOWTOs on the nsnam wiki for more details about how to get the
// system confgured.  For an example, have a look at "HOWTO Use Linux
// Containers to set up virtual networks" which uses this code as an
// example.
//
// The configuration you are after is explained in great detail in the
// HOWTO, but looks like the following:
//
//  +----------+                           +----------+
//  | virtual  |                           | virtual  |
//  |  Linux   |                           |  Linux   |
//  |   Host   |                           |   Host   |
//  |          |                           |          |
//  |   eth0   |                           |   eth0   |
//  +----------+                           +----------+
//       |                                      |
//  +----------+                           +----------+
//  |  Linux   |                           |  Linux   |
//  |  Bridge  |                           |  Bridge  |
//  +----------+                           +----------+
//       |                                      |
//  +------------+                       +-------------+
//  | "tap-left" |                       | "tap-right" |
//  +------------+                       +-------------+
//       |           n0            n1           |
//       |       +--------+    +--------+       |
//       +-------|  tap   |    |  tap   |-------+
//               | bridge |    | bridge |
//               +--------+    +--------+
//               |  CSMA  |    |  CSMA  |
//               +--------+    +--------+
//                   |             |
//                   |             |
//                   |             |
//                   ===============
//                      CSMA LAN
//
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/network-module.h"
#include "ns3/tap-bridge-module.h"
#include "ns3/mobility-module.h"


#include "boost/asio.hpp"
#include "boost/iostreams/filtering_streambuf.hpp"
#include "boost/iostreams/copy.hpp"
#include "boost/iostreams/filter/gzip.hpp"


#include <fstream>
#include <iostream>

#include "protobuf_msgs/network_update.pb.h"
#include "protobuf_msgs/channel_data.pb.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("TapCsmaVirtualMachineExample");

/**
 * \brief Updates the position of a node.
 *
 * \param node A pointer to the target node.
 * \param position A ns3::Vector containing the [x, y, z] position of the drone, with respect to the origin.
 */
void
SetNodePosition(Ptr<Node> node, Vector position)
{
    Ptr<MobilityModel> mobility = node->GetObject<MobilityModel>();
    mobility->SetPosition(position);

    // std::cout << node->GetId() << ": " << position << std::endl;
}

/**
 * \brief Compresses a string with the zip protocol.
 *
 * \param data The string to compress.
 * \return The compressed string.
 */
static std::string 
gzip_compress(const std::string& data)
{
std::stringstream compressed;
std::stringstream origin(data);

boost::iostreams::filtering_streambuf< boost::iostreams::input> in;
in.push(boost::iostreams::gzip_compressor());
in.push(origin);
boost::iostreams::copy(in, compressed);

return compressed.str();
}

/**
 * \brief Decompress a string with the zip protocol.
 *
 * \param data The compressed string to decompress.
 * \return The decompressed string.
 */
static std::string 
gzip_decompress(const std::string& data)
{
std::stringstream compressed(data);
std::stringstream decompressed;

boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
in.push(boost::iostreams::gzip_decompressor());
in.push(compressed);
boost::iostreams::copy(in, decompressed);

return decompressed.str();
}

/**
 * \brief Sends a message from a socket.
 *
 * \param sock The socket used to send the message.
 * \param str The string message to send.
 */
void 
send_one_message(boost::asio::local::stream_protocol::socket &sock, std::string str)
{
// Send Preamble
std::size_t response_size=str.size();
// static_cast<uint32_t>(response_size);
uint32_t send_length=htonl(response_size);
sock.send(boost::asio::buffer(&send_length,4));
// Send Message
sock.send(boost::asio::buffer(str.data(), str.size()));
}

/**
 * \brief Receives a message from a socket.
 * 
 * This function will block until the next message is received, read its header (first 4 bytes) 
 * and then read the content of the message and return it as a string.
 *
 * \param sock The socket on which to listen for the next message.
 * \return The received message as a std::string.
 */
std::string 
receive_one_message(boost::asio::local::stream_protocol::socket &sock)
{
// Read Preamble
uint32_t data_preamble[4];
size_t length = sock.receive(boost::asio::buffer(data_preamble, 4));
uint32_t receive_length=ntohl(*data_preamble);
// Read Message
char data[receive_length];
length = sock.receive(boost::asio::buffer(data, receive_length));
std::string data_string(data,length);

return data_string;
}

/**
 * \brief Generates the final protobuf message of protobuf_msgs/NetworkUpdate.
 * 
 * Usually, this function will be passed the [NetworkUpdate] protobuf message with message-type BEGIN from the Network coordinator. 
 * It will fill the message with the Bit Error Rate (BER) of each packet , 
 * change the message-type to END, string-serialize the protobuf message and return it to the network coordinator.
 * 
 * \param PhysicsUpdate_msg A protobuf message of type NetworkUpdate.
 * \return A string-serialized version of the updated protobuf message.
 */
std::string 
generate_response(network_update_proto::NetworkUpdate NetworkUpdate_msg)
{
    // Change message type to "END"
    NetworkUpdate_msg.set_msg_type(network_update_proto::NetworkUpdate::END);
    // for(int i=0 ; i < NetworkUpdate_msg.pkt_id_size() ; i++){
    //     NetworkUpdate_msg.add_rx_ip(NetworkUpdate_msg.dst_ip(i)); // Not dealing with broadcast
    // }
    // Transform the response to std::string
    std::string str_response;
    NetworkUpdate_msg.SerializeToString(&str_response);

    return str_response;
}


int
main(int argc, char* argv[])
{
    // CommandLine cmd(__FILE__);
    // cmd.Parse(argc, argv);

    //
    // We are interacting with the outside, real, world.  This means we have to
    // interact in real-time and therefore means we have to use the real-time
    // simulator and take the time to calculate checksums.
    //
    // GlobalValue::Bind("SimulatorImplementationType", StringValue("ns3::RealtimeSimulatorImpl"));
    // GlobalValue::Bind("ChecksumEnabled", BooleanValue(true));

    //
    // Create two ghost nodes.  The first will represent the virtual machine host
    // on the left side of the network; and the second will represent the VM on
    // the right side.
    //
    NodeContainer nodes;
    nodes.Create(2);

    //
    // Use a CsmaHelper to get a CSMA channel created, and the needed net
    // devices installed on both of the nodes.  The data rate and delay for the
    // channel can be set through the command-line parser.  For example,
    //
    // ./ns3 run "tap-csma-virtual-machine --ns3::CsmaChannel::DataRate=10000000"
    //
    CsmaHelper csma;
    NetDeviceContainer devices = csma.Install(nodes);

    //
    // Use the TapBridgeHelper to connect to the pre-configured tap devices for
    // the left side.  We go with "UseBridge" mode since the CSMA devices support
    // promiscuous mode and can therefore make it appear that the bridge is
    // extended into ns-3.  The install method essentially bridges the specified
    // tap to the specified CSMA device.
    //
    TapBridgeHelper tapBridge;
    tapBridge.SetAttribute("Mode", StringValue("UseLocal"));
    tapBridge.SetAttribute("DeviceName", StringValue("wifi_tap0"));
    tapBridge.Install(nodes.Get(0), devices.Get(0));

    //
    // Connect the right side tap to the right side CSMA device on the right-side
    // ghost node.
    //
    tapBridge.SetAttribute("DeviceName", StringValue("wifi_tap1"));
    tapBridge.Install(nodes.Get(1), devices.Get(1));


    // Create and connect UDS Socket
    boost::asio::io_service io_service;
    ::unlink("/tmp/net_server_socket");
    boost::asio::local::stream_protocol::endpoint ep("/tmp/net_server_socket");
    boost::asio::local::stream_protocol::acceptor acceptor(io_service, ep);
    boost::asio::local::stream_protocol::socket socket(io_service);
    acceptor.accept(socket);

    std::cout << "Network simulator ready" << std::endl;


    while(true) {
        std::string received_data = gzip_decompress(receive_one_message(socket));

        network_update_proto::NetworkUpdate NetworkUpdate_msg;
        NetworkUpdate_msg.ParseFromString(received_data);

        channel_data_proto::ChannelData channel_data; 
        if(!NetworkUpdate_msg.channel_data().empty()){
            channel_data.ParseFromString(gzip_decompress(NetworkUpdate_msg.channel_data()));

            // Verify that the number of positions (vectors of 7 values [x, y, z, qw, qx, qy, qz]) sent by the robotics simulator corresponds to the number of existing nodes in NS-3
            // Then, update the node's positions (orientation is ignored for now)
            if(nodes.GetN() != (uint32_t)channel_data.node_list_size()/7){
                
            } else {
                // for(uint32_t i=0 ; i < nodes.GetN() ; i++){
                //     Vector pos;
                //     pos.x = channel_data.node_list(7*i);
                //     pos.y = channel_data.node_list(7*i + 1);
                //     pos.z = channel_data.node_list(7*i + 2);
                //     SetNodePosition(nodes.Get(i), pos);
                // }
            }

        } else {
            // std::cout << "Network simulator received an update message with empty channel data" << std::endl;
        }
        
        // Once all the events are scheduled, advance W time in the simulation and stop
        Simulator::Stop(MilliSeconds(10));
        Simulator::Run();

        std::string response=gzip_compress(generate_response(NetworkUpdate_msg));

        // Send the response to the network coordinator
        send_one_message(socket, response);
    }

    Simulator::Destroy();

}
