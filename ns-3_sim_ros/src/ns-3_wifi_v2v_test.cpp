#include <iostream>

#include "boost/filesystem.hpp"

#include "rclcpp/rclcpp.hpp"
#include "boost/asio.hpp"
#include "boost/iostreams/filtering_streambuf.hpp"
#include "boost/iostreams/copy.hpp"
#include "boost/iostreams/filter/gzip.hpp"

#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/node-list.h"
#include "ns3/buildings-module.h"
#include "ns3/network-module.h"
#include "ns3/spectrum-signal-parameters.h"
#include "ns3/three-gpp-channel-model.h"
#include "ns3/three-gpp-spectrum-propagation-loss-model.h"
#include "ns3/three-gpp-v2v-propagation-loss-model.h"

#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/spectrum-wifi-helper.h"

#include "ns3/udp-client-server-helper.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"

#include "protobuf_msgs/network_update.pb.h"
#include "protobuf_msgs/channel_data.pb.h"

#include <yaml-cpp/yaml.h>

// Define a port on which the nodes listen and talk
#define PORT 80

using namespace ns3;
NS_LOG_COMPONENT_DEFINE("wifi_v2v_test");

static std::string m_output_file;

static Ptr<ThreeGppPropagationLossModel>
    m_propagationLossModel; //!< the PropagationLossModel object
static Ptr<ThreeGppSpectrumPropagationLossModel>
    m_spectrumLossModel;                       //!< the SpectrumPropagationLossModel object
static Ptr<ChannelConditionModel> m_condModel; //!< the ChannelConditionModel object

// Global variables for use in callbacks.
double g_signalDbmAvg; //!< Average signal power [dBm]
double g_noiseDbmAvg;  //!< Average noise power [dBm]
uint32_t g_samples;    //!< Number of samples
static std::vector<uint32_t> delivered_packets;

/**
 * Monitor sniffer Rx trace
 *
 * \param packet The sensed packet.
 * \param channelFreqMhz The channel frequency [MHz].
 * \param txVector The Tx vector.
 * \param aMpdu The aMPDU.
 * \param signalNoise The signal and noise dBm.
 * \param staId The STA ID.
 */
void
MonitorSniffRx(Ptr<const Packet> packet,
               uint16_t channelFreqMhz,
               WifiTxVector txVector,
               MpduInfo aMpdu,
               SignalNoiseDbm signalNoise,
               uint16_t staId)

{
    g_samples++;
    g_signalDbmAvg += ((signalNoise.signal - g_signalDbmAvg) / g_samples);
    g_noiseDbmAvg += ((signalNoise.noise - g_noiseDbmAvg) / g_samples);
    std::cout << "Sniffed Rx Signal Noise (dBm) " << signalNoise.signal << " / " << signalNoise.noise << std::endl;
}


Vector
GetPosition(Ptr<ns3::Node> node)
{
    Ptr<MobilityModel> mobility = node->GetObject<MobilityModel>();
    return mobility->GetPosition();
}


/**
 * \brief Print the pathloss value of a BuildingsPropagationLossModel every given period of simulated time
 * 
 * \param period Simulated time between each message
 * \param model A Ptr to the BuildingsPropagationLossModel
 * \param sender A Ptr to the sending Node
 * \param receiver A Ptr to the receiving Node
 */
void 
printPosition(Time period, Ptr<ns3::Node> sender, Ptr<ns3::Node> receiver){
    Vector pos_sender = GetPosition(sender);
    Vector pos_receiver = GetPosition(receiver);
    std::cout << "Sender: (" << pos_sender.x << " ; " << pos_sender.y << ")\nReceiver: (" << pos_receiver.x << " ; " << pos_receiver.y << ")\n";
    Simulator::Schedule(period,
                        &printPosition,
                        period,
                        sender,
                        receiver);
}


// A map holding the node <-> address of each robot
std::map<Ipv4Address, Ptr<Node>> ip_node_map;

/**
 * \brief Compresses a string with the zip protocol.
 *
 * \param data The string to compress.
 * \return The compressed string.
 */
static std::string 
gzip_compress(const std::string& data)
{
std::stringstream compressed;
std::stringstream origin(data);

boost::iostreams::filtering_streambuf< boost::iostreams::input> in;
in.push(boost::iostreams::gzip_compressor());
in.push(origin);
boost::iostreams::copy(in, compressed);

return compressed.str();
}

/**
 * \brief Decompress a string with the zip protocol.
 *
 * \param data The compressed string to decompress.
 * \return The decompressed string.
 */
static std::string 
gzip_decompress(const std::string& data)
{
std::stringstream compressed(data);
std::stringstream decompressed;

boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
in.push(boost::iostreams::gzip_decompressor());
in.push(compressed);
boost::iostreams::copy(in, decompressed);

return decompressed.str();
}

/**
 * \brief Receives a message from a socket.
 * 
 * This function will block until the next message is received, read its header (first 4 bytes) 
 * and then read the content of the message and return it as a string.
 *
 * \param sock The socket on which to listen for the next message.
 * \return The received message as a std::string.
 */
std::string 
receive_one_message(boost::asio::local::stream_protocol::socket &sock)
{
// Read Preamble
uint32_t data_preamble[4];
size_t length = sock.receive(boost::asio::buffer(data_preamble, 4));
uint32_t receive_length=ntohl(*data_preamble);
// Read Message
char data[receive_length];
length = sock.receive(boost::asio::buffer(data, receive_length));
std::string data_string(data,length);

return data_string;
}

/**
 * \brief Sends a message from a socket.
 *
 * \param sock The socket used to send the message.
 * \param str The string message to send.
 */
void 
send_one_message(boost::asio::local::stream_protocol::socket &sock, std::string str)
{
// Send Preamble
std::size_t response_size=str.size();
// static_cast<uint32_t>(response_size);
uint32_t send_length=htonl(response_size);
sock.send(boost::asio::buffer(&send_length,4));
// Send Message
sock.send(boost::asio::buffer(str.data(), str.size()));
}

/**
 * \brief Get the address of the first ipv4 interface of a node.
 *
 * \param node A pointer to the target node.
 */
Ipv4Address
GetAddressOfNode(Ptr<Node> node)
{
  Ptr<Ipv4> ipv4 = node->GetObject<Ipv4>();
  Ipv4InterfaceAddress iaddr = ipv4->GetAddress(1, 0);
  Ipv4Address addri = iaddr.GetAddress();
  return addri;
}

/**
 * Send an IP packet.
 *
 * \param sender The Node id of the source node.
 * \param ip_dest The ipv4 address of the destination as ns3::Ipv4Address object
 * \param pkt_size The size of the packet (in bytes?).
 */
static void
SendPacket(uint32_t sender, Ipv4Address ip_dest, uint32_t pkt_size, uint32_t pkt_id)
{
    // Get the pointers to the nodes
    Ptr<Node> nodeSender = NodeList::GetNode(sender);
    // At initialisation, each node was aggregated to a Socket object, get this Socket
    Ptr<Socket> socket = nodeSender->GetObject<Socket>();
    InetSocketAddress remote = InetSocketAddress(ip_dest, PORT);
    if(socket->Connect(remote) != -1)
    {
        NS_LOG_INFO("socket connected");
    } else {
        NS_LOG_INFO("Error when connecting socket");
    }

    if(pkt_size >= 4){
        uint8_t buffer[pkt_size] = {0};
        buffer[0] = pkt_id;
        buffer[1] = pkt_id >> 8,
        buffer[2] = pkt_id >> 16;
        buffer[3] = pkt_id >> 24;
        const uint8_t* buff = buffer;

        // Send a "fake" packet in the socket (second argument is the socket control flags)
        if(socket->Send(Create<Packet>(buff, pkt_size)))
        {
            NS_LOG_INFO("One packet sent");
        } else {
            NS_LOG_INFO("Error when sending a packet");
        }
    } else {
        NS_LOG_UNCOND("Error : the packet is smaller than 4 bytes.");
    }

}

/**
 * Function called when a packet is received.
 *
 * \param socket The receiving socket.
 */
void
ReceivePacket(Ptr<Socket> socket)
{
    Ptr<Packet> packet;
    while ((packet = socket->Recv()))
    {
        NS_LOG_UNCOND("Received one packet!");
        uint8_t* buffer;
        if(packet->Serialize(buffer, std::numeric_limits<uint32_t>::max()) == 1){
            for(int i=0 ; i != packet->GetSize() ;i++){
                std::cout << buffer[i] << std::endl;
            }
            std::cout << "--" << std::endl;
            uint32_t pkt_id = buffer[0] | (buffer[1] << 8) | (buffer[2] << 16) | (buffer[3] << 24);
            std::cout << "Packet received ! Pkt_id = " << pkt_id << std::endl;
            // delivered_packets.push_back(pkt_id);
        } else {
            NS_LOG_INFO("Error at reception of the packet, buffer issue.");
            std::cout << "error during serialization of the packet" << std::endl;
        }
        // std::cout << socket->GetNode() << " Received a packet!" << std::endl;
    }
}

/**
 * \brief Generates the final protobuf message of protobuf_msgs/NetworkUpdate.
 * 
 * Usually, this function will be passed the [NetworkUpdate] protobuf message with message-type BEGIN from the Network coordinator. 
 * It will fill the message with the Bit Error Rate (BER) of each packet , 
 * change the message-type to END, string-serialize the protobuf message and return it to the network coordinator.
 * 
 * \param PhysicsUpdate_msg A protobuf message of type NetworkUpdate.
 * \return A string-serialized version of the updated protobuf message.
 */
std::string 
generate_response(network_update_proto::NetworkUpdate NetworkUpdate_msg)
{
    // Change message type to "END"
    NetworkUpdate_msg.set_msg_type(network_update_proto::NetworkUpdate::END);
    for(int i=0 ; i < NetworkUpdate_msg.pkt_id_size() ; i++){
        if(!delivered_packets.empty()){
            if(std::find(delivered_packets.begin(), delivered_packets.end(), NetworkUpdate_msg.pkt_id(i)) != delivered_packets.end()){
                // This packet arrived at destination
                NetworkUpdate_msg.add_ber(1e-9);
                NetworkUpdate_msg.add_rx_ip(NetworkUpdate_msg.dst_ip(i)); // Not dealing with broadcast
            }
        }
    }
    // Transform the response to std::string
    std::string str_response;
    NetworkUpdate_msg.SerializeToString(&str_response);

    return str_response;
}


/**
* We declare a ROS2 Node here. It takes the form of a class which constructor will be called by rclcpp::spin().
* The use of ROS for the NS3 simulator is essentially to harmonize the code, and allow the use of a standard logging system (RCLCPP_INFO, etc.).
* We could very well have standard NS-3 code here.
*/
class Ns3Simulation : public rclcpp::Node
{
    public:
        Ns3Simulation() : Node("ns3_simulation")
        {
            // Declare two parameters for this ros2 node
            auto param_desc = rcl_interfaces::msg::ParameterDescriptor();
            param_desc.description = "Path to the YAML configuration file.";
            this->declare_parameter("config_file", "", param_desc);
            this->declare_parameter("verbose", false);

            // Fetch the parameter path to the config file using ros2 parameter
            std::string config_file_path = this->get_parameter("config_file").get_parameter_value().get<std::string>();

            // Verify existence of the config file, abort if not found
            if(access(config_file_path.c_str(), F_OK) != 0){
               RCLCPP_ERROR(this->get_logger(), "The config file was not found at : %s", config_file_path.c_str());
               exit(EXIT_FAILURE);
            }

             // Parse the config file
            YAML::Node config = YAML::LoadFile(config_file_path);

            // Create a folder based on the experience name, if not existant already
            std::string experience_name = config["experience_name"].as<std::string>();
            if(boost::filesystem::create_directories("./data/"+experience_name)){
                RCLCPP_DEBUG(this->get_logger(), "Created a new data folder for this experience : %s", experience_name.c_str());
            } else {
                RCLCPP_DEBUG(this->get_logger(), "Using existing data folder for this experiment");
            }

            // Define ne output file name, based on the existing files in the experience folder
            std::string temp_path;
            int i = 1;
            while(m_output_file.empty()){
                temp_path = "./data/"+experience_name+"/out_"+std::to_string(i)+".csv";
                if(boost::filesystem::exists(temp_path)){
                    i++;
                } else {
                    m_output_file = temp_path;
                }
            }

            // initialize the output file with headers
            std::ofstream f;
            f.open(m_output_file.c_str(), std::ios::out);
            f << "Time[s],TxPosX[m],TxPosY[m],RxPosX[m],RxPosY[m],ChannelState,SNR[dB],Pathloss[dB]"
            << std::endl;
            f.close();


// ========================= NS3 CONFIGURATION =========================

            double frequency = 5e9;          // operating frequency in Hz
            // double txPow_dbm = 30.0;            // tx power in dBm
            // double noiseFigure = 9.0;           // noise figure in dB
            double simTime = 60;         // simulation time
            Time timeRes = MilliSeconds(100);    // time resolution
            std::string scenario = "V2V-Urban"; // 3GPP propagation scenario, V2V-Urban or V2V-Highway
            double vScatt = 0;                  // maximum speed of the vehicles in the scenario [m/s]
            // double subCarrierSpacing = 60e3;    // subcarrier spacing in kHz
            // uint32_t numRb = 275;               // number of resource blocks
            std::string errorModelType = "ns3::NistErrorRateModel";
            std::string wifiType = "ns3::SpectrumWifiPhy";
            std::string phyMode("HtMcs1");   // Define a "Phy mode" that will be given to the WifiRemoteStationManager
            double datarate = 14.4;            //The data rate associated with the configuration index 9 of examples/wireless/wifi-spectrum-per-example.cc
            // double distance = 3;                    // Distance in m between the nodes at initialization
            uint32_t numNodes = config["robots_number"].as<int>();
            // bool wifiVerbose = false;               // Turns on/off logging for ALL the wifi-related ns3 modules
            // bool tracing = false;
            // Fetch the length of one step in the robotics simulator (in ms) and the number of iterations between two synchronization 
            // and compute the window size, i.e. the time between two synchronizations with the robotics simulator
            uint32_t window_size = config["steps_per_window"].as<uint32_t>() * config["step_length"].as<uint32_t>()*1000; // in micro-seconds
            uint32_t payloadSize = 20; // for UDP (1000 bytes IPv4)
            

            // Create the nodes
            NodeContainer nodes;
            nodes.Create(numNodes);

    // **************** BUILDINGS MODULE ****************

            // [Buildings] -- Define buildings with their center (x, y), and their size in the three dimensions : (size_x, size_y, height)
            std::vector< Ptr<Building> > buildings;
            for(auto building : config["buildings"]){
                double x_min = building["x"].as<int>() - (building["size_x"].as<int>() / 2);
                double x_max = building["x"].as<int>() + (building["size_x"].as<int>() / 2);
                double y_min = building["y"].as<int>() - (building["size_y"].as<int>() / 2);
                double y_max = building["y"].as<int>() + (building["size_y"].as<int>() / 2);
                double z_min = 0.0;
                double z_max = building["height"].as<int>();
                Ptr<Building> build = CreateObject<Building>();
                build->SetBoundaries(Box(x_min, x_max, y_min, y_max, z_min, z_max));
                build->SetBuildingType(Building::Office);
                build->SetExtWallsType(Building::ConcreteWithWindows);
                build->SetNFloors(1);
                build->SetNRoomsX(1);
                build->SetNRoomsY(1);
                buildings.push_back(build);
                RCLCPP_INFO(this->get_logger(), "Created a building with corners (%f, %f, %f, %f)", x_min, x_max, y_min, y_max);
            }
    
    // **************** MOBILITY MODULE ****************

            /*      0-----------30m
             *      /----------------------\
             *      |                      |
             *      o-----<-----o          |  30 m
             *      |           |          |  |
             *      |           ^          |  |
             *      |           |          |  |
             *      |           |          |  |
             *      \-----------o----------/  0
            */
/*            Ptr<MobilityModel> txMob;
            Ptr<MobilityModel> rxMob;

            // 3GPP defines that the maximum speed in urban scenario is 60 km/h
            vScatt = 60 / 3.6;

            // set the mobility model
            double vTx = 3; // m/s
            txMob = CreateObject<WaypointMobilityModel>();
            rxMob = CreateObject<ConstantPositionMobilityModel>();

            rxMob->GetObject<ConstantPositionMobilityModel>()->SetPosition(Vector(1.0, 1.0, 20.0));

            Time nextWaypoint = Seconds(0.0);
            txMob->GetObject<WaypointMobilityModel>()->AddWaypoint(
                Waypoint(nextWaypoint, Vector(0.0, 0.0, 20.0)));
            nextWaypoint += Seconds(30 / vTx);
            txMob->GetObject<WaypointMobilityModel>()->AddWaypoint(
                Waypoint(nextWaypoint, Vector(30.0, 0.0, 20.0)));
            nextWaypoint += Seconds(30 / vTx);
            txMob->GetObject<WaypointMobilityModel>()->AddWaypoint(
                Waypoint(nextWaypoint, Vector(30.0, 30.0, 20.0)));
            nextWaypoint += Seconds(30 / vTx);
            txMob->GetObject<WaypointMobilityModel>()->AddWaypoint(
                Waypoint(nextWaypoint, Vector(0.0, 30.0, 20.0)));
            nextWaypoint += Seconds(30 / vTx);
            txMob->GetObject<WaypointMobilityModel>()->AddWaypoint(
                Waypoint(nextWaypoint, Vector(0.0, 0.0, 20.0)));
            

            nodes.Get(0)->AggregateObject(txMob);
            nodes.Get(1)->AggregateObject(rxMob);
*/
            // Ptr<MobilityModel> txMob;
            // Ptr<MobilityModel> rxMob;
            // Ptr<MobilityModel> node3Mob;


            // txMob = CreateObject<ConstantPositionMobilityModel>();
            // rxMob = CreateObject<ConstantPositionMobilityModel>();
            // node3Mob = CreateObject<ConstantPositionMobilityModel>();

            // txMob->GetObject<ConstantPositionMobilityModel>()->SetPosition(Vector(0.0, 0.0, 20.0));
            // rxMob->GetObject<ConstantPositionMobilityModel>()->SetPosition(Vector(1.0, 1.0, 20.0));
            // node3Mob->GetObject<ConstantPositionMobilityModel>()->SetPosition(Vector(2.0, 2.0, 20.0));

            // nodes.Get(0)->AggregateObject(txMob);
            // nodes.Get(1)->AggregateObject(rxMob);
            // nodes.Get(2)->AggregateObject(node3Mob);

            for(int i = 0; i < numNodes; i++){
                Ptr<MobilityModel> nodeMob;
                nodeMob = CreateObject<ConstantPositionMobilityModel>();
                nodeMob->GetObject<ConstantPositionMobilityModel>()->SetPosition(Vector(i, i, 20.0));
                nodes.Get(i)->AggregateObject(nodeMob);
            }

    // **************** PROPAGATION MODULE ****************


            YansWifiPhyHelper phy;
            SpectrumWifiPhyHelper spectrumPhy;
        // --------- YANS ---------
            if (wifiType == "ns3::YansWifiPhy"){
                YansWifiChannelHelper channel;
                channel.AddPropagationLoss("ns3::HybridBuildingsPropagationLossModel",
                                            "Frequency",     // Additional loss for each internal wall [dB] 
                                            DoubleValue(frequency),       // Default 5
                                            "ShadowSigmaExtWalls",  // Standard deviation of the normal distribution used to calculate the shadowing due to ext walls 
                                            DoubleValue(5.0),       // Default 5
                                            "ShadowSigmaIndoor",    // Standard deviation of the normal distribution used to calculate the shadowing for indoor nodes
                                            DoubleValue(8.0),       // Default 8
                                            "ShadowSigmaOutdoor",   // Standard deviation of the normal distribution used to calculate the shadowing for outdoor nodes
                                            DoubleValue(7.0));       // Default 7
                channel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
                phy.SetChannel(channel.Create());
                phy.Set("TxPowerStart", DoubleValue(1)); // dBm (1.26 mW)
                phy.Set("TxPowerEnd", DoubleValue(1));

            }
        // --------- SPECTRUM ---------
            else if (wifiType == "ns3::SpectrumWifiPhy"){

                Ptr<MultiModelSpectrumChannel> spectrumChannel = CreateObject<MultiModelSpectrumChannel>();
                
                // create the channel condition model
                m_condModel = CreateObject<ThreeGppV2vUrbanChannelConditionModel>();
                m_condModel->SetAttribute("UpdatePeriod", TimeValue(MilliSeconds(100)));

                // create the propagation loss model
                m_propagationLossModel = CreateObject<ThreeGppV2vUrbanPropagationLossModel>();
                m_propagationLossModel->SetAttribute("Frequency", DoubleValue(frequency));
                m_propagationLossModel->SetAttribute("ShadowingEnabled", BooleanValue(false));
                m_propagationLossModel->SetAttribute("ChannelConditionModel", PointerValue(m_condModel));

                spectrumChannel->AddPropagationLossModel(m_propagationLossModel);

                Ptr<ConstantSpeedPropagationDelayModel> delayModel = CreateObject<ConstantSpeedPropagationDelayModel>();

                spectrumChannel->SetPropagationDelayModel(delayModel);

                spectrumPhy.SetChannel(spectrumChannel);
                spectrumPhy.SetErrorRateModel(errorModelType);
                spectrumPhy.Set("TxPowerStart", DoubleValue(1)); // dBm  (1.26 mW)
                spectrumPhy.Set("TxPowerEnd", DoubleValue(1));
            }
            else {
                NS_FATAL_ERROR("Unsupported WiFi type " << wifiType);
            }

    // **************** WIFI MODULE ****************
            WifiHelper wifi;
            wifi.SetStandard(WIFI_STANDARD_80211n);
            WifiMacHelper mac;

            wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                                        "DataMode",
                                        StringValue(phyMode),
                                        "ControlMode",
                                        StringValue(phyMode));

            NetDeviceContainer devices;

            mac.SetType("ns3::AdhocWifiMac");

            if (wifiType == "ns3::YansWifiPhy")
            {
                devices = wifi.Install(phy, mac, nodes);
            } 
            else if (wifiType == "ns3::SpectrumWifiPhy") 
            {
                devices = wifi.Install(spectrumPhy, mac, nodes);
            }

            Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/"
            "ShortGuardIntervalSupported",
            BooleanValue(true));
            

            // [Buildings] -- Aggregate the building module to the nodes, so that we can use BuildingsPropagationLossModels with them
            BuildingsHelper::Install(nodes);

            // Enable OLSR
            OlsrHelper olsr;
            Ipv4StaticRoutingHelper staticRouting;

            // Define static and olsr routing protocols : higher priority is used first
            Ipv4ListRoutingHelper list;
            list.Add(staticRouting, 0);
            list.Add(olsr, 10);



            // Install the Internet stack on the nodes. This aggregates IPV4, IPV6, UDP and TCP to the nodes.
            InternetStackHelper internet;
            internet.SetRoutingHelper(list); // has effect on the next Install ()
            internet.Install(nodes);

            // Set IP adresses for the nodes.
            Ipv4AddressHelper ipv4;
            NS_LOG_INFO("Assign IP Addresses.");
            std::string ip_network = config["ip_network"].as<std::string>();
            std::string ip_mask = config["ip_mask"].as<std::string>();
            ipv4.SetBase(ip_network.c_str(), ip_mask.c_str());
            Ipv4InterfaceContainer ip = ipv4.Assign(devices);

            // multicast target
            const std::string targetAddr = "239.255.0.1";

            // add static routes for each node / device
            for (auto diter = devices.Begin(); diter != devices.End(); ++diter)
            {
                Ptr<ns3::Node> node = (*diter)->GetNode();

                // route for forwarding
                staticRouting.AddMulticastRoute(node,
                                                Ipv4Address::GetAny(),
                                                targetAddr.c_str(),
                                                *diter,
                                                NetDeviceContainer(*diter));
            }

/*    // **************** APPLICATION MODULE ****************
            ApplicationContainer serverApp;

            // UDP flow
            uint16_t port = 9;
            UdpServerHelper server(port);
            serverApp = server.Install(nodes.Get(0));
            serverApp.Start(Seconds(0.0));
            serverApp.Stop(Seconds(simTime + 1));

            UdpClientHelper client(ip.GetAddress(1), port);
            client.SetAttribute("MaxPackets", UintegerValue(4294967295U));
            client.SetAttribute("Interval", TimeValue(Time("0.01s"))); // packets/s
            client.SetAttribute("PacketSize", UintegerValue(payloadSize));
            ApplicationContainer clientApp = client.Install(nodes.Get(1));
            clientApp.Start(Seconds(1.0));
            clientApp.Stop(Seconds(simTime + 1));
*/

    // **************** SOCKET MODULE ****************
            // Create a socket on each node, 
            // Bind it to the IP address of the node 
            // Aggregate the two objects (node and socket together) <-- allows to call GetObject() on each other
            // Fill the ip-to-node map
            // Print info
            TypeId tid = TypeId::LookupByName("ns3::UdpSocketFactory");
            for(uint32_t i=0 ; i < numNodes ; i++)
            {
                Ptr<Socket> recvSink = Socket::CreateSocket(nodes.Get(i), tid);
                InetSocketAddress local = InetSocketAddress(Ipv4Address::GetAny(), 80);
                recvSink->Bind(local);
                recvSink->SetRecvCallback(MakeCallback(&ReceivePacket));
                nodes.Get(i)->AggregateObject(recvSink);
                ip_node_map.emplace(ip.GetAddress(i), nodes.Get(i));
                std::cout << "IP of node " << i << " : " << ip.GetAddress(i) << std::endl;
            }

    // **************** UDS SOCKET FOR NETWORK COORDINATOR ****************
            //Create and connect UDS Socket
            boost::asio::io_service io_service;
            ::unlink("/tmp/net_server_socket");
            boost::asio::local::stream_protocol::endpoint ep("/tmp/net_server_socket");
            boost::asio::local::stream_protocol::acceptor acceptor(io_service, ep);
            boost::asio::local::stream_protocol::socket socket(io_service);
            acceptor.accept(socket);
            RCLCPP_INFO(this->get_logger(), "finished setting up UDS socket");



            // Simulator::Schedule(Seconds(1.0),
            //         &printPosition,
            //         MilliSeconds(1000),
            //         nodes.Get(0),
            //         nodes.Get(1));

            int num_packets_to_nodes = 0;
            int num_packets_to_other_addresses = 0;
            int num_packets_to_multicast = 0;

    // **************** MAIN SIMULATION LOOP ****************
            while(true){
                // Wait until reception of a message on the UDS socket
                std::string received_data = gzip_decompress(receive_one_message(socket));
                // Initialize empty protobuf message type [NetworkUpdate]
                network_update_proto::NetworkUpdate NetworkUpdate_msg;
                // Transform the message received from the UDS socket [string] -> [protobuf]
                NetworkUpdate_msg.ParseFromString(received_data);

                // std::cout << "Proto received from Network coordinator: \n" << NetworkUpdate_msg.DebugString() << std::endl;

                channel_data_proto::ChannelData channel_data; 
                if(!NetworkUpdate_msg.channel_data().empty()){
                    channel_data.ParseFromString(gzip_decompress(NetworkUpdate_msg.channel_data()));
                }

                // Schedule the messages of the NetworkUpdate in NS-3
                for(int i=0 ; i < NetworkUpdate_msg.pkt_id_size() ; i++){
                    // Filter only the messages between robots
                    for(const auto& n : ip_node_map)
                    {
                        // Verify that the destination ip is a "known" ip ; i.e. it exists in our node map
                        if(n.first.Get() == NetworkUpdate_msg.dst_ip(i)){
                            Simulator::ScheduleNow(&SendPacket, 
                                    ip_node_map[Ipv4Address(NetworkUpdate_msg.src_ip(i))]->GetId(), // the NetworkUpdate_msg.src_ip is a IPv4 addr as uint32
                                    Ipv4Address(NetworkUpdate_msg.dst_ip(i)), // we construct a ns3::Ipv4Address object 
                                    NetworkUpdate_msg.pkt_lengths(i),
                                    NetworkUpdate_msg.pkt_id(i)); // the pkt length is already a uint32_t
                            num_packets_to_nodes++;
                        } else if(Ipv4Address(NetworkUpdate_msg.dst_ip(i)).IsMulticast()) {
                            // "multicast is broadcast"
                            Simulator::ScheduleNow(&SendPacket, 
                                    ip_node_map[Ipv4Address(NetworkUpdate_msg.src_ip(i))]->GetId(), // the NetworkUpdate_msg.src_ip is a IPv4 addr as uint32
                                    n.first, // we send the message to every node if the destination is a multicast
                                    NetworkUpdate_msg.pkt_lengths(i),
                                    NetworkUpdate_msg.pkt_id(i)); // the pkt length is already a uint32_t
                            num_packets_to_multicast++;
                        }
                        num_packets_to_other_addresses++;
                        // else {
                            // uint32_t ip_int = ntohl(NetworkUpdate_msg.dst_ip(i));
                            // char str[INET_ADDRSTRLEN];
                            
                            // inet_ntop(AF_INET, &ip_int, str, INET_ADDRSTRLEN);

                            // std::cerr << "Ignoring a packet destined to " << str << std::endl;
                        // }
                    }
                }

                // Once all the events are scheduled, advance W time in the simulation and stop
                Simulator::Stop(MicroSeconds(window_size));
                Simulator::Run();
                std::string response=gzip_compress(generate_response(NetworkUpdate_msg));

                if(this->get_parameter("verbose").get_parameter_value().get<bool>()){
                    // 32 = Green :)
                    RCLCPP_INFO(this->get_logger(), "\x1b[32m[%f] Advanced %i milliseconds\x1b[0m", Simulator::Now().GetSeconds(), window_size);
                }

                // Send the response to the network coordinator
                send_one_message(socket, response);

                // print the number of packets sent to each group of node:
                // std::cout << "\rNumber of packets sent to nodes / other addresses: " << num_packets_to_nodes << " / " << num_packets_to_other_addresses << std::endl;
            }

        }
};

/**
 * \brief The main function, spins the ROS2 Node
*/
int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<Ns3Simulation>());
    rclcpp::shutdown();
    return 0;
}
