#include <iostream>
#include <fstream>
#include <chrono>
#include <vector>

#include "boost/filesystem.hpp"

#include "rclcpp/rclcpp.hpp"

#include "ns3/core-module.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/mobility-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/log.h"
#include "ns3/node-list.h"
#include "ns3/buildings-module.h"

#include "ns3/network-module.h"
#include "ns3/spectrum-signal-parameters.h"
#include "ns3/three-gpp-channel-model.h"
#include "ns3/three-gpp-spectrum-propagation-loss-model.h"
#include "ns3/three-gpp-v2v-propagation-loss-model.h"
#include "ns3/uniform-planar-array.h"

// #include <boost/iostreams/filtering_streambuf.hpp>
// #include <boost/iostreams/copy.hpp>
// #include <boost/iostreams/filter/gzip.hpp>


#include "protobuf_msgs/network_update.pb.h"
#include "protobuf_msgs/channel_data.pb.h"

#include <yaml-cpp/yaml.h>

/*
* Adapted from NS3 example file : /ns-3-allinone/ns-3.37/examples/wireless/wifi-simple-adhoc-grid.cc
* and D'Urso's GzUav gzuav-0.2/ns-3/external-sync/examples/wifi-adhoc.cc
*/

// Define a port on which the nodes listen and talk
#define PORT 80

using namespace ns3;
NS_LOG_COMPONENT_DEFINE("AdhocTest");

static std::string output_file;

static Ptr<ThreeGppPropagationLossModel>
    m_propagationLossModel; //!< the PropagationLossModel object
static Ptr<ThreeGppSpectrumPropagationLossModel>
    m_spectrumLossModel;                       //!< the SpectrumPropagationLossModel object
static Ptr<ChannelConditionModel> m_condModel; //!< the ChannelConditionModel object


/**
 * \brief Updates the position of a node.
 *
 * \param node A pointer to the target node.
 * \param position A ns3::Vector containing the [x, y, z] position of the drone, with respect to the origin.
 */
void
SetPosition(Ptr<Node> node, Vector position)
{
    Ptr<MobilityModel> mobility = node->GetObject<MobilityModel>();
    mobility->SetPosition(position);

    // std::cout << node->GetId() << ": " << position << std::endl;
}

/*
 * \brief A structure that holds the parameters for the ComputeSnr
 * function. In this way the problem with the limited
 * number of parameters of method Schedule is avoided.
 */
struct ComputeSnrParams
{
    Ptr<MobilityModel> txMob;               //!< the tx mobility model
    Ptr<MobilityModel> rxMob;               //!< the rx mobility model
    Ptr<SpectrumSignalParameters> txParams; //!< the params of the tx signal
    double noiseFigure;                     //!< the noise figure in dB
    Ptr<PhasedArrayModel> txAntenna;        //!< the tx antenna array
    Ptr<PhasedArrayModel> rxAntenna;        //!< the rx antenna array
};

/**
 * Perform the beamforming using the DFT beamforming method
 * \param thisDevice the device performing the beamforming
 * \param thisAntenna the antenna object associated to thisDevice
 * \param otherDevice the device towards which point the beam
 */
static void
DoBeamforming(Ptr<NetDevice> thisDevice,
              Ptr<PhasedArrayModel> thisAntenna,
              Ptr<NetDevice> otherDevice)
{
    PhasedArrayModel::ComplexVector antennaWeights;

    // retrieve the position of the two devices
    Vector aPos = thisDevice->GetNode()->GetObject<MobilityModel>()->GetPosition();
    Vector bPos = otherDevice->GetNode()->GetObject<MobilityModel>()->GetPosition();

    // compute the azimuth and the elevation angles
    Angles completeAngle(bPos, aPos);

    PhasedArrayModel::ComplexVector bf = thisAntenna->GetBeamformingVector(completeAngle);
    thisAntenna->SetBeamformingVector(bf);
}

/**
 * Compute the average SNR
 * \param params A structure that holds a bunch of parameters needed by ComputSnr function to
 * calculate the average SNR
 */
static void
ComputeSnr(const ComputeSnrParams& params)
{
    // check the channel condition
    Ptr<ChannelCondition> cond = m_condModel->GetChannelCondition(params.txMob, params.rxMob);

    // apply the pathloss
    double propagationGainDb = m_propagationLossModel->CalcRxPower(0, params.txMob, params.rxMob);
    NS_LOG_DEBUG("Pathloss " << -propagationGainDb << " dB");
    double propagationGainLinear = std::pow(10.0, (propagationGainDb) / 10.0);
    *(params.txParams->psd) *= propagationGainLinear;

    // apply the fast fading and the beamforming gain
    Ptr<SpectrumValue> rxPsd = m_spectrumLossModel->CalcRxPowerSpectralDensity(params.txParams,
                                                                               params.txMob,
                                                                               params.rxMob,
                                                                               params.txAntenna,
                                                                               params.rxAntenna);
    NS_LOG_DEBUG("Average rx power " << 10 * log10(Sum(*rxPsd) * 180e3) << " dB");

    // create the noise psd
    // taken from lte-spectrum-value-helper
    const double kT_dBm_Hz = -174.0; // dBm/Hz
    double kT_W_Hz = std::pow(10.0, (kT_dBm_Hz - 30) / 10.0);
    double noiseFigureLinear = std::pow(10.0, params.noiseFigure / 10.0);
    double noisePowerSpectralDensity = kT_W_Hz * noiseFigureLinear;
    Ptr<SpectrumValue> noisePsd = Create<SpectrumValue>(params.txParams->psd->GetSpectrumModel());
    (*noisePsd) = noisePowerSpectralDensity;

    // compute the SNR
    NS_LOG_DEBUG("Average SNR " << 10 * log10(Sum(*rxPsd) / Sum(*noisePsd)) << " dB");

    // print the SNR and pathloss values in the snr-trace.txt file
    std::ofstream f;
    f.open(output_file.c_str(), std::ios::out | std::ios::app);
    f << Simulator::Now().GetSeconds() << "," // time [s]
      << params.txMob->GetPosition().x << "," << params.txMob->GetPosition().y << ","
      << params.rxMob->GetPosition().x << "," << params.rxMob->GetPosition().y << ","
      << cond->GetLosCondition() << ","                  // channel state
      << 10 * log10(Sum(*rxPsd) / Sum(*noisePsd)) << "," // SNR [dB]
      << -propagationGainDb << std::endl;                // pathloss [dB]
    f.close();
}


/**
* We declare a ROS2 Node here. It takes the form of a class which constructor will be called by rclcpp::spin().
* The use of ROS for the NS3 simulator is essentially to harmonize the code, and allow the use of a standard logging system (RCLCPP_INFO, etc.).
* We could very well have standard NS-3 code here.
*/
class Ns3Simulation : public rclcpp::Node
{
    public:
        Ns3Simulation() : Node("ns3_simulation")
        {
            // Declare two parameters for this ros2 node
            auto param_desc = rcl_interfaces::msg::ParameterDescriptor();
            param_desc.description = "Path to the YAML configuration file.";
            this->declare_parameter("config_file", "", param_desc);
            this->declare_parameter("verbose", false);

            // Fetch the parameter path to the config file using ros2 parameter
            std::string config_file_path = this->get_parameter("config_file").get_parameter_value().get<std::string>();

            // Verify existence of the config file, abort if not found
            if(access(config_file_path.c_str(), F_OK) != 0){
               RCLCPP_ERROR(this->get_logger(), "The config file was not found at : %s", config_file_path.c_str());
               exit(EXIT_FAILURE);
            }

             // Parse the config file
            YAML::Node config = YAML::LoadFile(config_file_path);
            
            std::string experience_name = config["experience_name"].as<std::string>();
            if(boost::filesystem::create_directories("./data/"+experience_name)){
                RCLCPP_DEBUG(this->get_logger(), "Created a new data folder for this experience : %s", experience_name.c_str());
            } else {
                RCLCPP_DEBUG(this->get_logger(), "Using existing data folder for this experiment");
            }

            std::string temp_path;
            int i = 1;
            while(output_file.empty()){
                temp_path = "./data/"+experience_name+"/out_"+std::to_string(i)+".csv";
                if(boost::filesystem::exists(temp_path)){
                    i++;
                } else {
                    output_file = temp_path;
                }
            }

            // initialize the output file
            std::ofstream f;
            f.open(output_file.c_str(), std::ios::out);
            f << "Time[s],TxPosX[m],TxPosY[m],RxPosX[m],RxPosY[m],ChannelState,SNR[dB],Pathloss[dB]"
            << std::endl;
            f.close();

            double frequency = 5e9;          // operating frequency in Hz
            double txPow_dbm = 30.0;            // tx power in dBm
            double noiseFigure = 9.0;           // noise figure in dB
            Time simTime = Seconds(20);         // simulation time
            Time timeRes = MilliSeconds(10);    // time resolution
            std::string scenario = "V2V-Urban"; // 3GPP propagation scenario, V2V-Urban or V2V-Highway
            double vScatt = 0;                  // maximum speed of the vehicles in the scenario [m/s]
            double subCarrierSpacing = 60e3;    // subcarrier spacing in kHz
            uint32_t numRb = 275;               // number of resource blocks


            // [Buildings] -- Define buildings with their center (x, y), and their size in the three dimensions : (size_x, size_y, height)
            std::vector< Ptr<Building> > buildings;
            for(auto building : config["buildings"]){
                double x_min = building["x"].as<int>() - (building["size_x"].as<int>() / 2);
                double x_max = building["x"].as<int>() + (building["size_x"].as<int>() / 2);
                double y_min = building["y"].as<int>() - (building["size_y"].as<int>() / 2);
                double y_max = building["y"].as<int>() + (building["size_y"].as<int>() / 2);
                double z_min = 0.0;
                double z_max = building["height"].as<int>();
                Ptr<Building> build = CreateObject<Building>();
                build->SetBoundaries(Box(x_min, x_max, y_min, y_max, z_min, z_max));
                build->SetBuildingType(Building::Office);
                build->SetExtWallsType(Building::ConcreteWithWindows);
                build->SetNFloors(1);
                build->SetNRoomsX(1);
                build->SetNRoomsY(1);
                buildings.push_back(build);
                RCLCPP_INFO(this->get_logger(), "Created a building with corners (%f, %f, %f, %f)", x_min, x_max, y_min, y_max);
            }

            // Create the nodes
            NodeContainer nodes;
            nodes.Create(2);

            // create the tx and rx devices
            Ptr<SimpleNetDevice> txDev = CreateObject<SimpleNetDevice>();
            Ptr<SimpleNetDevice> rxDev = CreateObject<SimpleNetDevice>();


            // associate the nodes and the devices
            nodes.Get(0)->AddDevice(txDev);
            txDev->SetNode(nodes.Get(0));
            nodes.Get(1)->AddDevice(rxDev);
            rxDev->SetNode(nodes.Get(1));

            // create the antenna objects and set their dimensions
            Ptr<PhasedArrayModel> txAntenna =
                CreateObjectWithAttributes<UniformPlanarArray>("NumColumns",
                                                            UintegerValue(2),
                                                            "NumRows",
                                                            UintegerValue(2),
                                                            "BearingAngle",
                                                            DoubleValue(-M_PI / 2));
            Ptr<PhasedArrayModel> rxAntenna =
                CreateObjectWithAttributes<UniformPlanarArray>("NumColumns",
                                                            UintegerValue(2),
                                                            "NumRows",
                                                            UintegerValue(2),
                                                            "BearingAngle",
                                                            DoubleValue(M_PI / 2));

            Ptr<MobilityModel> txMob;
            Ptr<MobilityModel> rxMob;

            // 3GPP defines that the maximum speed in urban scenario is 60 km/h
            vScatt = 60 / 3.6;

            // set the mobility model
            double vTx = 6; // m/s
            txMob = CreateObject<WaypointMobilityModel>();
            rxMob = CreateObject<ConstantPositionMobilityModel>();

            rxMob->GetObject<ConstantPositionMobilityModel>()->SetPosition(Vector(1.0, 1.0, 20.0));

            Time nextWaypoint = Seconds(0.0);
            txMob->GetObject<WaypointMobilityModel>()->AddWaypoint(
                Waypoint(nextWaypoint, Vector(0.0, 0.0, 20.0)));
            nextWaypoint += Seconds(30 / vTx);
            txMob->GetObject<WaypointMobilityModel>()->AddWaypoint(
                Waypoint(nextWaypoint, Vector(30.0, 0.0, 20.0)));
            nextWaypoint += Seconds(30 / vTx);
            txMob->GetObject<WaypointMobilityModel>()->AddWaypoint(
                Waypoint(nextWaypoint, Vector(30.0, 30.0, 20.0)));
            nextWaypoint += Seconds(30 / vTx);
            txMob->GetObject<WaypointMobilityModel>()->AddWaypoint(
                Waypoint(nextWaypoint, Vector(0.0, 30.0, 20.0)));
            nextWaypoint += Seconds(30 / vTx);
            txMob->GetObject<WaypointMobilityModel>()->AddWaypoint(
                Waypoint(nextWaypoint, Vector(0.0, 0.0, 20.0)));
            

            nodes.Get(0)->AggregateObject(txMob);
            nodes.Get(1)->AggregateObject(rxMob);

            // [Buildings] -- Aggregate the building module to the nodes, so that we can use BuildingsPropagationLossModels with them
            BuildingsHelper::Install(nodes);

            // create the channel condition model
            m_condModel = CreateObject<ThreeGppV2vUrbanChannelConditionModel>();

            // create the propagation loss model
            m_propagationLossModel = CreateObject<ThreeGppV2vUrbanPropagationLossModel>();

            m_condModel->SetAttribute("UpdatePeriod", TimeValue(MilliSeconds(100)));

            m_propagationLossModel->SetAttribute("Frequency", DoubleValue(frequency));
            m_propagationLossModel->SetAttribute("ShadowingEnabled", BooleanValue(false));
            m_propagationLossModel->SetAttribute("ChannelConditionModel", PointerValue(m_condModel));

            // create the channel model
            Ptr<ThreeGppChannelModel> channelModel = CreateObject<ThreeGppChannelModel>();
            channelModel->SetAttribute("Scenario", StringValue(scenario));
            channelModel->SetAttribute("Frequency", DoubleValue(frequency));
            channelModel->SetAttribute("ChannelConditionModel", PointerValue(m_condModel));
            channelModel->SetAttribute("vScatt", DoubleValue(vScatt));

            // create the spectrum propagation loss model
            m_spectrumLossModel = CreateObjectWithAttributes<ThreeGppSpectrumPropagationLossModel>(
                "ChannelModel",
                PointerValue(channelModel));

            // set the beamforming vectors
            DoBeamforming(txDev, txAntenna, rxDev);
            DoBeamforming(rxDev, rxAntenna, txDev);


            // create the tx power spectral density
            Bands rbs;
            double freqSubBand = frequency;
            for (uint32_t n = 0; n < numRb; ++n)
            {
                BandInfo rb;
                rb.fl = freqSubBand;
                freqSubBand += subCarrierSpacing / 2;
                rb.fc = freqSubBand;
                freqSubBand += subCarrierSpacing / 2;
                rb.fh = freqSubBand;
                rbs.push_back(rb);
            }
            Ptr<SpectrumModel> spectrumModel = Create<SpectrumModel>(rbs);
            Ptr<SpectrumValue> txPsd = Create<SpectrumValue>(spectrumModel);
            Ptr<SpectrumSignalParameters> txParams = Create<SpectrumSignalParameters>();
            double txPow_w = std::pow(10., (txPow_dbm - 30) / 10);
            double txPowDens = (txPow_w / (numRb * subCarrierSpacing));
            (*txPsd) = txPowDens;
            txParams->psd = txPsd->Copy();

            for (int i = 0; i < simTime / timeRes; i++)
            {
                ComputeSnrParams params{txMob, rxMob, txParams, noiseFigure, txAntenna, rxAntenna};
                Simulator::Schedule(timeRes * i, &ComputeSnr, params);
            }

            Simulator::Run();
            Simulator::Destroy();
            exit(EXIT_SUCCESS);
            
        }

    private:
        void AdvancePosition(Ptr<ns3::Node>);
        Vector GetPosition(Ptr<ns3::Node>);

};

void
Ns3Simulation::AdvancePosition(Ptr<ns3::Node> node)
{
    Vector pos = GetPosition(node);
    pos.y += 0.1;
    if (pos.y >= 210.0)
    {
        return;
    }
    SetPosition(node, pos);
    Simulator::Schedule(Seconds(0.1), &Ns3Simulation::AdvancePosition, this, node);
}

Vector
Ns3Simulation::GetPosition(Ptr<ns3::Node> node)
{
    Ptr<MobilityModel> mobility = node->GetObject<MobilityModel>();
    return mobility->GetPosition();
}

/**
 * \brief The main function, spins the ROS2 Node
*/
int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<Ns3Simulation>());
    rclcpp::shutdown();
    return 0;
}