// Adapted from ns-3.29/src/tap-bridge/examples/tap-wifi-virtual-machine.cc

#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-module.h"
#include "ns3/tap-bridge-module.h"
#include "ns3/buildings-module.h"

#include "ns3/spectrum-signal-parameters.h"
#include "ns3/three-gpp-channel-model.h"
#include "ns3/three-gpp-spectrum-propagation-loss-model.h"
#include "ns3/three-gpp-v2v-propagation-loss-model.h"

#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/spectrum-wifi-helper.h"



// network devices, do not exceed COUNT in nns_setup.py
static const int COUNT=2;

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("LxcNs3Wifi");

static Ptr<ThreeGppPropagationLossModel> m_propagationLossModel;        //!< the PropagationLossModel object
static Ptr<ThreeGppSpectrumPropagationLossModel> m_spectrumLossModel;   //!< the SpectrumPropagationLossModel object
static Ptr<ChannelConditionModel> m_condModel;                          //!< the ChannelConditionModel object

int 
main (int argc, char *argv[])
{
  int step_length = 10; // ms

  CommandLine cmd;
  cmd.AddValue("step_length", "Length of one iteration in ms", step_length);
  cmd.Parse (argc, argv);


  //
  // We are interacting with the outside, real, world.  This means we have to 
  // interact in real-time and therefore means we have to use the real-time
  // simulator and take the time to calculate checksums.
  //
  // GlobalValue::Bind ("SimulatorImplementationType", StringValue ("ns3::RealtimeSimulatorImpl"));
  // GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

  //
  // Create two ghost nodes.  The first will represent the virtual machine host
  // on the left side of the network; and the second will represent the VM on 
  // the right side.
  //
  NodeContainer nodes;
  nodes.Create (COUNT);

  //
  // We're going to use 802.11 A so set up a wifi helper to reflect that.
  //
  WifiHelper wifi;
  wifi.SetStandard (WIFI_STANDARD_80211g);
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("ErpOfdmRate54Mbps"));
  // wifi.SetRemoteStationManager("ns3::IdealWifiManager");

  //
  // No reason for pesky access points, so we'll use an ad-hoc network.
  //
  WifiMacHelper wifiMac;
  wifiMac.SetType ("ns3::AdhocWifiMac");

  std::string channelType = "Spectrum"; // ["Yans", "Spectrum"]

  //
  // Configure two wifi channels with different models.
  //
  YansWifiChannelHelper YansChannel = YansWifiChannelHelper::Default();


  Ptr<MultiModelSpectrumChannel> spectrumChannel = CreateObject<MultiModelSpectrumChannel>();
  
  // create the channel condition model
  m_condModel = CreateObject<ThreeGppV2vUrbanChannelConditionModel>();
  m_condModel->SetAttribute("UpdatePeriod", TimeValue(MilliSeconds(100)));

  // create the propagation loss model
  m_propagationLossModel = CreateObject<ThreeGppV2vUrbanPropagationLossModel>();
  m_propagationLossModel->SetAttribute("Frequency", DoubleValue(2.4e9));
  m_propagationLossModel->SetAttribute("ShadowingEnabled", BooleanValue(false));
  m_propagationLossModel->SetAttribute("ChannelConditionModel", PointerValue(m_condModel));

  spectrumChannel->AddPropagationLossModel(m_propagationLossModel);

  Ptr<ConstantSpeedPropagationDelayModel> delayModel = CreateObject<ConstantSpeedPropagationDelayModel>();

  spectrumChannel->SetPropagationDelayModel(delayModel);


  //
  // Configure two wifi physical layers, and assign it to their respective channels
  //
  YansWifiPhyHelper YansWifiPhy;
  YansWifiPhy.SetChannel(YansChannel.Create ());

  SpectrumWifiPhyHelper spectrumWifiPhy;
  spectrumWifiPhy.SetChannel(spectrumChannel);
  spectrumWifiPhy.SetErrorRateModel("ns3::NistErrorRateModel");
  spectrumWifiPhy.Set("TxPowerStart", DoubleValue(1)); // dBm  (1.26 mW)
  spectrumWifiPhy.Set("TxPowerEnd", DoubleValue(1));



  //
  // Install the wireless devices onto our ghost nodes.
  //
  NetDeviceContainer devices;
  if(channelType == "Yans"){
    devices = wifi.Install (YansWifiPhy, wifiMac, nodes);
  } else if(channelType == "Spectrum"){
    devices = wifi.Install (spectrumWifiPhy, wifiMac, nodes);
  } else {
    NS_LOG_WARN("Unknown channel type");
    exit;
  }

  //
  // We need location information since we are talking about wifi, so add a
  // constant position to the ghost nodes.
  //
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  for (int i=0; i<COUNT; i++) {
    positionAlloc->Add (Vector (5*i, 0.0, 0.0));
  }
  mobility.SetPositionAllocator (positionAlloc);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (nodes);

  // [Buildings] -- Aggregate the building module to the nodes, so that we can use BuildingsPropagationLossModels with them
  BuildingsHelper::Install(nodes);


  //
  // Use the TapBridgeHelper to connect to the pre-configured tap devices for 
  // the left side.  We go with "UseLocal" mode since the wifi devices do not
  // support promiscuous mode (because of their natures0.  This is a special
  // case mode that allows us to extend a linux bridge into ns-3 IFF we will
  // only see traffic from one other device on that bridge.  That is the case
  // for this configuration.
  //
  TapBridgeHelper tapBridge;
  tapBridge.SetAttribute ("Mode", StringValue ("UseLocal"));
  char buffer[10];
  for (int i=0; i<COUNT; i++) {
    sprintf(buffer, "wifi_tap%d", i);
    tapBridge.SetAttribute ("DeviceName", StringValue(buffer));
    tapBridge.Install (nodes.Get(i), devices.Get(i));
  }

  //
  // Run the simulation for a year, use CTRL-C to stop.
  //
  while(true){
    Simulator::Stop(MilliSeconds(step_length)); // Stop after step_length second
    Simulator::Run();
    std::cout << "\x1b[32m["<<Simulator::Now()<<"] Advanced "<<step_length<<" milliseconds\x1b[0m" << std::endl;
    // std::this_thread::sleep_for(std::chrono::milliseconds(step_length));

  }
  // Simulator::Stop(Seconds(60*60));
  // std::cout << "Running the simulator for 1 hour." << std::endl;
  // Simulator::Run();
  // Simulator::Destroy ();
}