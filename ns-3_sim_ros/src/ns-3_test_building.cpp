#include <iostream>
#include <fstream>
#include <chrono>
#include <vector>

#include "rclcpp/rclcpp.hpp"

#include "ns3/core-module.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/mobility-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/log.h"
#include "ns3/node-list.h"
#include "ns3/buildings-module.h"

#include <boost/asio.hpp>

#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>

#include <boost/filesystem.hpp>

#include "protobuf_msgs/network_update.pb.h"
#include "protobuf_msgs/channel_data.pb.h"

#include <yaml-cpp/yaml.h>

/*
* Adapted from NS3 example file : /ns-3-allinone/ns-3.37/examples/wireless/wifi-simple-adhoc-grid.cc
* and D'Urso's GzUav gzuav-0.2/ns-3/external-sync/examples/wifi-adhoc.cc
*/

// Define a port on which the nodes listen and talk
#define PORT 80

using namespace ns3;
NS_LOG_COMPONENT_DEFINE("AdhocTest");

// A map holding the node <-> address of each robot
std::map<Ipv4Address, Ptr<Node>> ip_node_map;

/**
 * \brief Compresses a string with the zip protocol.
 *
 * \param data The string to compress.
 * \return The compressed string.
 */
static std::string 
gzip_compress(const std::string& data)
{
std::stringstream compressed;
std::stringstream origin(data);

boost::iostreams::filtering_streambuf< boost::iostreams::input> in;
in.push(boost::iostreams::gzip_compressor());
in.push(origin);
boost::iostreams::copy(in, compressed);

return compressed.str();
}

/**
 * \brief Decompress a string with the zip protocol.
 *
 * \param data The compressed string to decompress.
 * \return The decompressed string.
 */
static std::string 
gzip_decompress(const std::string& data)
{
std::stringstream compressed(data);
std::stringstream decompressed;

boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
in.push(boost::iostreams::gzip_decompressor());
in.push(compressed);
boost::iostreams::copy(in, decompressed);

return decompressed.str();
}

/**
 * \brief Receives a message from a socket.
 * 
 * This function will block until the next message is received, read its header (first 4 bytes) 
 * and then read the content of the message and return it as a string.
 *
 * \param sock The socket on which to listen for the next message.
 * \return The received message as a std::string.
 */
std::string 
receive_one_message(boost::asio::local::stream_protocol::socket &sock)
{
// Read Preamble
uint32_t data_preamble[4];
size_t length = sock.receive(boost::asio::buffer(data_preamble, 4));
uint32_t receive_length=ntohl(*data_preamble);
// Read Message
char data[receive_length];
length = sock.receive(boost::asio::buffer(data, receive_length));
std::string data_string(data,length);

return data_string;
}

/**
 * \brief Sends a message from a socket.
 *
 * \param sock The socket used to send the message.
 * \param str The string message to send.
 */
void 
send_one_message(boost::asio::local::stream_protocol::socket &sock, std::string str)
{
// Send Preamble
std::size_t response_size=str.size();
// static_cast<uint32_t>(response_size);
uint32_t send_length=htonl(response_size);
sock.send(boost::asio::buffer(&send_length,4));
// Send Message
sock.send(boost::asio::buffer(str.data(), str.size()));
}

/**
 * \brief Updates the position of a node.
 *
 * \param node A pointer to the target node.
 * \param position A ns3::Vector containing the [x, y, z] position of the drone, with respect to the origin.
 */
void
SetPosition(Ptr<Node> node, Vector position)
{
    Ptr<MobilityModel> mobility = node->GetObject<MobilityModel>();
    mobility->SetPosition(position);

    // std::cout << node->GetId() << ": " << position << std::endl;
}

/**
 * \brief Get the address of the first ipv4 interface of a node.
 *
 * \param node A pointer to the target node.
 */
Ipv4Address
GetAddressOfNode(Ptr<Node> node)
{
  Ptr<Ipv4> ipv4 = node->GetObject<Ipv4>();
  Ipv4InterfaceAddress iaddr = ipv4->GetAddress(1, 0);
  Ipv4Address addri = iaddr.GetAddress();
  return addri;
}

/**
 * Send an IP packet.
 *
 * \param sender The Node id of the source node.
 * \param ip_dest The ipv4 address of the destination as ns3::Ipv4Address object
 * \param pkt_size The size of the packet (in bytes?).
 */
static void
SendPacket(uint32_t sender, Ipv4Address ip_dest, uint32_t pkt_size)
{
    // Get the pointers to the nodes
    Ptr<Node> nodeSender = NodeList::GetNode(sender);
    // At initialisation, each node was aggregated to a Socket object, get this Socket
    Ptr<Socket> socket = nodeSender->GetObject<Socket>();
    InetSocketAddress remote = InetSocketAddress(ip_dest, PORT);
    if(socket->Connect(remote) != -1)
    {
        NS_LOG_INFO("socket connected");
    } else {
        NS_LOG_INFO("Error when connecting socket");
    }

    // Send a "fake" packet in the socket (second argument is the socket control flags)
    if(socket->Send(Create<Packet>(pkt_size)))
    {
        NS_LOG_INFO("One packet sent");
    } else {
        NS_LOG_INFO("Error when sending a packet");
    }
}

/**
 * Function called when a packet is received.
 *
 * \param socket The receiving socket.
 */
void
ReceivePacket(Ptr<Socket> socket)
{
    while (socket->Recv())
    {
        NS_LOG_UNCOND("Received one packet!");
        std::cout << socket->GetNode() << " Received a packet!" << std::endl;
    }
}

/**
 * \brief Generates the final protobuf message of protobuf_msgs/NetworkUpdate.
 * 
 * Usually, this function will be passed the [NetworkUpdate] protobuf message with message-type BEGIN from the Network coordinator. 
 * It will fill the message with the Bit Error Rate (BER) of each packet , 
 * change the message-type to END, string-serialize the protobuf message and return it to the network coordinator.
 * 
 * \param PhysicsUpdate_msg A protobuf message of type NetworkUpdate.
 * \return A string-serialized version of the updated protobuf message.
 */
std::string 
generate_response(network_update_proto::NetworkUpdate NetworkUpdate_msg)
{
    // Change message type to "END"
    NetworkUpdate_msg.set_msg_type(network_update_proto::NetworkUpdate::END);
    for(int i=0 ; i < NetworkUpdate_msg.pkt_id_size() ; i++){
        NetworkUpdate_msg.add_ber(1e-9);
        NetworkUpdate_msg.add_rx_ip(NetworkUpdate_msg.dst_ip(i)); // Not dealing with broadcast
    }
    // Transform the response to std::string
    std::string str_response;
    NetworkUpdate_msg.SerializeToString(&str_response);

    return str_response;
}

void savePathlossData(double position, double pathloss){
    std::chrono::time_point now = std::chrono::system_clock::now();
    std::time_t tt = std::chrono::system_clock::to_time_t(now);

    std::tm local_tm = *localtime(&tt);

    std::fstream file;
    std::string path = "/home/theotime/simulation_ws/data/" + std::to_string(local_tm.tm_year+1900) + "-" + std::to_string(local_tm.tm_mon+1) + "-" + std::to_string(local_tm.tm_mday);
    std::string filename = "/"+ std::to_string(local_tm.tm_hour)+"h"+std::to_string(local_tm.tm_min)+":"+std::to_string(local_tm.tm_sec)+"_pathloss.csv";

    if(boost::filesystem::create_directories(path)){
        std::cout << "Created directory for data storage : " << path << std::endl;
    }
    if(!boost::filesystem::exists(path+filename)){
        file.open(path+filename, std::ios::out);
        file << "position,pathloss" << "\n";
    } else {
        file.open(path+filename, std::fstream::app);
    }

    file << std::to_string(position)+","+std::to_string(pathloss) << "\n";

    file.close();
}

/**
* We declare a ROS2 Node here. It takes the form of a class which constructor will be called by rclcpp::spin().
* The use of ROS for the NS3 simulator is essentially to harmonize the code, and allow the use of a standard logging system (RCLCPP_INFO, etc.).
* We could very well have standard NS-3 code here.
*/
class Ns3Simulation : public rclcpp::Node
{
    public:
        Ns3Simulation() : Node("ns3_simulation")
        {
            // Declare two parameters for this ros2 node
            auto param_desc = rcl_interfaces::msg::ParameterDescriptor();
            param_desc.description = "Path to the YAML configuration file.";
            this->declare_parameter("config_file", "", param_desc);
            this->declare_parameter("verbose", false);

            // Fetch the parameter path to the config file using ros2 parameter
            std::string config_file_path = this->get_parameter("config_file").get_parameter_value().get<std::string>();

            // Verify existence of the config file, abort if not found
            if(access(config_file_path.c_str(), F_OK) != 0){
               RCLCPP_ERROR(this->get_logger(), "The config file was not found at : %s", config_file_path.c_str());
               exit(EXIT_FAILURE);
            }

             // Parse the config file
            YAML::Node config = YAML::LoadFile(config_file_path);

            // Define a "Phy mode" that will be given to the WifiRemoteStationManager
            std::string phyMode("DsssRate1Mbps");
            double distance = 3;      // m
            uint32_t numNodes = config["robots_number"].as<int>();
            // Turns on/off logging for ALL the wifi-related ns3 modules
            bool wifiVerbose = false;
            bool tracing = false;
            // Fetch the length of one step in the robotics simulator (in ms) and the number of iterations between two synchronization 
            // and compute the window size, i.e. the time between two synchronizations with the robotics simulator
            uint32_t window_size = config["steps_per_window"].as<uint32_t>() * config["step_length"].as<uint32_t>()*1000; // in micro-seconds

            // [Buildings] -- Define buildings with their center (x, y), and their size in the three dimensions : (size_x, size_y, height)
            std::vector< Ptr<Building> > buildings;
            for(auto building : config["buildings"]){
                double x_min = building["x"].as<int>() - (building["size_x"].as<int>() / 2);
                double x_max = building["x"].as<int>() + (building["size_x"].as<int>() / 2);
                double y_min = building["y"].as<int>() - (building["size_y"].as<int>() / 2);
                double y_max = building["y"].as<int>() + (building["size_y"].as<int>() / 2);
                double z_min = 0.0;
                double z_max = building["height"].as<int>();
                Ptr<Building> build = CreateObject<Building>();
                build->SetBoundaries(Box(x_min, x_max, y_min, y_max, z_min, z_max));
                build->SetBuildingType(Building::Office);
                build->SetExtWallsType(Building::ConcreteWithWindows);
                // build->SetNFloors(6);
                // build->SetNRoomsX(2);
                // build->SetNRoomsY(2);
                buildings.push_back(build);
                RCLCPP_INFO(this->get_logger(), "Created a building with corners (%f, %f, %f, %f)", x_min, x_max, y_min, y_max);
            }

            // Fix non-unicast data rate to be the same as that of unicast
            Config::SetDefault("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue(phyMode));

            // Create the nodes
            NodeContainer nodes;
            nodes.Create(numNodes);

            // The below set of helpers will help us to put together the wifi NICs we want
            WifiHelper wifi;
            if (wifiVerbose)
            {
                wifi.EnableLogComponents(); // Turn on all Wifi logging
            }

            YansWifiPhyHelper wifiPhy;
            // set it to zero; otherwise, gain will be added
            wifiPhy.Set("RxGain", DoubleValue(0));
            // ns-3 supports RadioTap and Prism tracing extensions for 802.11b
            wifiPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);

            YansWifiChannelHelper wifiChannel;
            wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
            // wifiChannel.AddPropagationLoss("ns3::FriisPropagationLossModel"); 
            wifiChannel.AddPropagationLoss("ns3::ThreeGppV2vUrbanPropagationLossModel",
                                            "Frequency",     // Additional loss for each internal wall [dB] 
                                            DoubleValue(frequency),       // Default 5
                                            "ShadowSigmaExtWalls",  // Standard deviation of the normal distribution used to calculate the shadowing due to ext walls 
                                            DoubleValue(5.0),       // Default 5
                                            "ShadowSigmaIndoor",    // Standard deviation of the normal distribution used to calculate the shadowing for indoor nodes
                                            DoubleValue(8.0),       // Default 8
                                            "ShadowSigmaOutdoor",   // Standard deviation of the normal distribution used to calculate the shadowing for outdoor nodes
                                            DoubleValue(7.0));       // Default 7
            
            // We can't retrieve the propagation models of a YansWifiChannelHelper, so we create a copy of it. 
            Ptr<HybridBuildingsPropagationLossModel> myModel = CreateObject<HybridBuildingsPropagationLossModel>();
            myModel->SetAttribute("InternalWallLoss", DoubleValue(5.0));
            myModel->SetAttribute("ShadowSigmaExtWalls", DoubleValue(5.0));
            myModel->SetAttribute("ShadowSigmaIndoor", DoubleValue(8.0));
            myModel->SetAttribute("ShadowSigmaOutdoor", DoubleValue(7.0));

            // Associate our channel to the phy layer
            wifiPhy.SetChannel(wifiChannel.Create());

            // Add an upper mac and disable rate control
            WifiMacHelper wifiMac;
            wifi.SetStandard(WIFI_STANDARD_80211b);
            wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                                        "DataMode",
                                        StringValue(phyMode),
                                        "ControlMode",
                                        StringValue(phyMode));
            // Set it to adhoc mode
            wifiMac.SetType("ns3::AdhocWifiMac");
            NetDeviceContainer devices = wifi.Install(wifiPhy, wifiMac, nodes);

            // Instantiate the mobility model for the nodes. The initial positions don't really matter : at first iteration of the co-simulator, 
            // theses values will be over-written by the positions of the robots in Gazebo
            MobilityHelper mobility;
            mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                        "MinX",
                                        DoubleValue(0.0),
                                        "MinY",
                                        DoubleValue(0.0),
                                        "DeltaX",
                                        DoubleValue(distance),
                                        "DeltaY",
                                        DoubleValue(distance),
                                        "GridWidth",
                                        UintegerValue(5),
                                        "LayoutType",
                                        StringValue("RowFirst"),
                                        "Z",
                                        DoubleValue(1.0));
            mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
            mobility.Install(nodes);

            // [Buildings] -- Aggregate the building module to the nodes, so that we can use BuildingsPropagationLossModels with them
            BuildingsHelper::Install(nodes);

            // Enable OLSR
            OlsrHelper olsr;
            Ipv4StaticRoutingHelper staticRouting;

            // Define static and olsr routing protocols : higher priority is used first
            Ipv4ListRoutingHelper list;
            list.Add(staticRouting, 0);
            list.Add(olsr, 10);

            // Install the Internet stack on the nodes. This aggregates IPV4, IPV6, UDP and TCP to the nodes.
            InternetStackHelper internet;
            internet.SetRoutingHelper(list); // has effect on the next Install ()
            internet.Install(nodes);

            // Set IP adresses for the nodes.
            Ipv4AddressHelper ipv4;
            NS_LOG_INFO("Assign IP Addresses.");
            std::string ip_network = config["ip_network"].as<std::string>();
            std::string ip_mask = config["ip_mask"].as<std::string>();
            ipv4.SetBase(ip_network.c_str(), ip_mask.c_str());
            Ipv4InterfaceContainer ip = ipv4.Assign(devices);

            // Create a socket on each node, 
            // Bind it to the IP address of the node 
            // Aggregate the two objects (node and socket together) <-- allows to call GetObject() on each other
            // Fill the ip-to-node map
            // Print info
            TypeId tid = TypeId::LookupByName("ns3::UdpSocketFactory");
            for(uint32_t i=0 ; i < numNodes ; i++)
            {
                Ptr<Socket> recvSink = Socket::CreateSocket(nodes.Get(i), tid);
                InetSocketAddress local = InetSocketAddress(Ipv4Address::GetAny(), 80);
                recvSink->Bind(local);
                recvSink->SetRecvCallback(MakeCallback(&ReceivePacket));
                nodes.Get(i)->AggregateObject(recvSink);
                ip_node_map.emplace(ip.GetAddress(i), nodes.Get(i));
                std::cout << "IP of node " << i << " : " << ip.GetAddress(i) << std::endl;
            }

            if (tracing == true)
            {
                AsciiTraceHelper ascii;
                wifiPhy.EnableAsciiAll(ascii.CreateFileStream("wifi-simple-adhoc-grid.tr"));
                wifiPhy.EnablePcap("wifi-simple-adhoc-grid", devices);
                // Trace routing tables
                Ptr<OutputStreamWrapper> routingStream =
                    Create<OutputStreamWrapper>("wifi-simple-adhoc-grid.routes", std::ios::out);
                olsr.PrintRoutingTableAllEvery(Seconds(2), routingStream);
                Ptr<OutputStreamWrapper> neighborStream =
                    Create<OutputStreamWrapper>("wifi-simple-adhoc-grid.neighbors", std::ios::out);
                olsr.PrintNeighborCacheAllEvery(Seconds(2), neighborStream);

                // To do-- enable an IP-level trace that shows forwarding events only
            }

            // Schedule recursive events to print pathloss between node 0 and 1 every 1s (simulated). First event at 2s.
            Simulator::Schedule(Seconds(10.0),
                                &Ns3Simulation::printPathloss,
                                this,
                                MilliSeconds(100),
                                myModel,
                                nodes.Get(0),
                                nodes.Get(1));


            Simulator::Schedule(Seconds(10),
                                &Ns3Simulation::AdvancePosition,
                                this,
                                nodes.Get(0));

            Simulator::Schedule(Seconds(10),
                                &Ns3Simulation::printIndoor,
                                this,
                                Seconds(1),
                                nodes.Get(0));

            Simulator::Stop(Seconds(60));
            Simulator::Run();
            
        }

    private:
        void printPathloss(Time, Ptr<HybridBuildingsPropagationLossModel>, Ptr<ns3::Node>, Ptr<ns3::Node>);
        void AdvancePosition(Ptr<ns3::Node>);
        Vector GetPosition(Ptr<ns3::Node>);
        void printIndoor(Time, Ptr<ns3::Node>);

};

/**
 * \brief Print the pathloss value of a BuildingsPropagationLossModel every given period of simulated time
 * 
 * \param period Simulated time between each message
 * \param model A Ptr to the BuildingsPropagationLossModel
 * \param sender A Ptr to the sending Node
 * \param receiver A Ptr to the receiving Node
 */
void 
Ns3Simulation::printPathloss(Time period, Ptr<HybridBuildingsPropagationLossModel> model, Ptr<ns3::Node> sender, Ptr<ns3::Node> receiver){
    double loss;
    Vector pos = GetPosition(sender);
    loss = model->GetLoss(sender->GetObject<MobilityModel>(), receiver->GetObject<MobilityModel>());
    RCLCPP_INFO(this->get_logger(), "pos(y) %f : %f dBm", pos.y, loss);
    savePathlossData(pos.y, loss);
    Simulator::Schedule(period,
                        &Ns3Simulation::printPathloss,
                        this,
                        period,
                        model,
                        sender,
                        receiver);
}

void 
Ns3Simulation::printIndoor(Time period, Ptr<ns3::Node> node){
    bool indoor = node->GetObject<MobilityBuildingInfo>()->IsIndoor();
    RCLCPP_INFO(this->get_logger(), "[%i] is indoor %d", node->GetId(), indoor);
    Simulator::Schedule(period,
                        &Ns3Simulation::printIndoor,
                        this,
                        period,
                        node);
}

void
Ns3Simulation::AdvancePosition(Ptr<ns3::Node> node)
{
    Vector pos = GetPosition(node);
    pos.y += 0.1;
    if (pos.y >= 210.0)
    {
        return;
    }
    SetPosition(node, pos);
    Simulator::Schedule(Seconds(0.1), &Ns3Simulation::AdvancePosition, this, node);
}

Vector
Ns3Simulation::GetPosition(Ptr<ns3::Node> node)
{
    Ptr<MobilityModel> mobility = node->GetObject<MobilityModel>();
    return mobility->GetPosition();
}

/**
 * \brief The main function, spins the ROS2 Node
*/
int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<Ns3Simulation>());
    rclcpp::shutdown();
    return 0;
}